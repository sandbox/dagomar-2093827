
-- SUMMARY --

The ratiocrop module provides an image effect that crops the image based on
ratio. It only crops one value, width or height, so the image is the biggest
crop possible with this ratio. It also provides an option for crop
position.


-- REQUIREMENTS --

None.


-- INSTALLATION --

* Install as usual, see http://drupal.org/node/895232 for further information.


-- CONFIGURATION --

* Add or edit a style at:
  Home » Administration » Configuration » Media » Image styles

  - Add Ratiocrop as an effect
  - Choose width, height values and set crop position.


-- CONTACT --

Current maintainers:
* Dagomar Paulides (dagomar) - http://drupal.org/user/174986/
